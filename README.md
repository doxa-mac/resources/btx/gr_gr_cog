
# btx gr_gr_cog 
Biblical translation files used by Doxa.

The files are © Common Orthodox Liturgical Greek

The files in this repository are formatted for use with Doxa.

Use of these files is subject to the terms stated in the LICENSE file.

[Doxa](https://doxa.liml.org) is a liturgical software product from the [Orthodox Christian Mission Center](https://ocmc.org). 

[Doxa install link](https://github.com/liturgiko/doxa/releases)
